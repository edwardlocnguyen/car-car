# CarCar

A car dealership management tool used to handle services and sales.

### Team:

* Edward Nguyen - Services
* Alex Adams - Sales

## Design

Our project is broken up into three domains:

* Inventory
* Services
* Sales

### Inventory

The Inventory holds the data of all manufacturers, models, and automobiles offered by the dealership. A user can create instances of a manufacturer, vehicle model, and automobile in the Inventory. Each backend model has a foreign key attribute that links all models together. The Sales and Services domains use a polling function to collect data from the Inventory, which is then used to carry out various functions within their respective microservices.

### Services

The Services domain contains three backend models, the technician, appointment, and an automobile value object. Each technician contains a name and employee number attirbute. Each appointment contains a vehicle VIN number, customer name, date and time of appointment, reason for service, an in-progress/completed boolean field, a VIP status boolean field (to determine whether the car was purchased at the dealership or not), and a foreign key to a technician. The Automobile value object contains the vehicle VIN number - this data is retrieved from the Automobile model in the Inventory microservice via a polling function. The retrieved VIN number is used to determine the VIP status.

Users can create, cancel, complete, and assign technicians to service appointments. Users can also search for a car's service history by filtering for its VIN number.

### Sales

The Sales domain contains four backend models, the employee sales person, customer, vehicle sale, and an automobile value object. Each sales person contains a name and employee number attribute. Each customer contains a name, address, and phone number attribute. Each sale has a price attribute and foreign keys to the sales person, customer, and automobile value object. The automobile value object is retrieved from the Automobile model in the Inventory microservice via a polling function. Each retrieved automobile instance is required for the creation of a sale instance.

Users can create new sales records as well as search for historical records filtered by a sales person.

## Screenshots

![Home Page](/source/images/home.png)*Home Page*

![Services Page](/source/images/service.png)*Services Page*

![Sales Page](/source/images/sales.png)*Sales Page*