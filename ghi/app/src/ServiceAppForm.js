import { useNavigate } from 'react-router-dom';
import React from 'react';
import './App.css';

class ServiceAppForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        vin: '',
        customer: '',
        date_time: '',
        reason: '',
        complete: false,
        technician_id: '',
        technicians: [],
      };
      this.handleStateChange = this.handleStateChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    
  // to show technician drop down in form
  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';
    // purpose of async and await?
    // no async/await, then the fetch fxn returns a promise bc program needs time to fetch data
    // async/await allows fxn to return a response (of the promise)
    // .json() is returning another promise from the response
    // a second await is required to return a response (of the second promise)
    const response = await fetch(url);
    
    if (response.ok) {
      const data = await response.json();
      this.setState({technicians: data.technicians});
    }
  }

  // when would async be necessary here, like the other handle event fxns?
  handleStateChange(event) {
    const id = event.target.id;
    const value = event.target.value;
    this.setState({[id]: value})
    // need square brackets to reference id variable
  }
    
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.technicians;
    const url = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      const cleared = {
        vin: '',
        customer: '',
        date_time: '',
        reason: '',
        complete: false,
        technician_id: '',
      };
      this.setState(cleared);
    }
  }

  // cannot use hooks in classes
  // classes have state, functional components don't have this and need hooks to manipulate states

  render() {
    return (
        <>
          <button
            className="btn btn-outline-light"
            name="/services/"
            onClick={this.props.handleNavigate}
            // accessing the handleNavigate key in the props object
            >Back to Services Home
          </button>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.vin} onChange={this.handleStateChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.customer} onChange={this.handleStateChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={this.state.date_time} onChange={this.handleStateChange} placeholder="Date Time" type="datetime-local" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">Date / Time</label>
                        </div>
                        <div className="mb-3">
                            <select value={this.state.technician_id} onChange={this.handleStateChange} required name="technician_id" id="technician_id" className="form-select">
                            <option value="">Choose a technician</option>
                            {this.state.technicians.map(technician => {
                                return (
                                <option key={technician.id} value={technician.id}>
                                    {technician.name}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="reason">Reason</label>
                            <textarea value={this.state.reason} onChange={this.handleStateChange} rows="3" name="reason" id="reason" className="form-control"></textarea>
                        </div>
                        <button className="btn btn-outline-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
  }
}

export default ServiceAppForm;
