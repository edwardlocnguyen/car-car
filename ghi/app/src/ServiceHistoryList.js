import { useState, useEffect } from "react";
import './App.css';
import { useNavigate } from "react-router-dom";

function ServiceHistoryList() {
    const [appointments, setAppointments] = useState([])
    const [searchTerm, setSearchTerm] = useState('')
    // [state variable, how to change the state variable]
    // useState = initializes the state

    const url = 'http://localhost:8080/api/appointments/'

    const fetchAppointments = async () => {
        const res = await fetch(url)
        const appointmentsJSON = await res.json()
        setAppointments(appointmentsJSON.appointments);
    }
    
    useEffect(() => {
        fetchAppointments()
    }, [])

    const navigate = useNavigate()
    const handleNavigate = (event) => {
        event.preventDefault();
        navigate(event.target.name);
    }

    const search = () => {
        // the appointment instance/state has a vin attribute, we want to compare that to the search
        // if vin value has search value, then filter the list
        return appointments.map(eachApp => {
            return (eachApp.complete && searchTerm.toLowerCase().includes(eachApp.vin.toLowerCase().slice(0,searchTerm.length)))?
            (<tr key={eachApp.id}>
                <td>{eachApp.vin}</td>
                <td>{eachApp.customer}</td>
                <td>{eachApp.date_time.slice(0, 10)}</td>
                <td>{eachApp.date_time.slice(11, 16)}</td>
                <td>{eachApp.technician.name}</td>
                <td>{eachApp.reason}</td>
             </tr>
            ) : ""
        })
    }

    return (
        <>  
            <button
                className="btn btn-outline-light"
                name="/services/"
                onClick={handleNavigate}
                >Back to Services Home
            </button>
            <input className="form-control mr-sm-2" type="text" placeholder="Search VIN..." onChange={event => {setSearchTerm(event.target.value)}}/>
            <div className="offset-0.5">
                <div className="shadow p-4 mt-4">
                    <h1>Service History</h1>
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Customer Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                            </tr>
                        </thead>
                        <tbody>
                            {search()}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}

export default ServiceHistoryList;