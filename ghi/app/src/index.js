import React from 'react';
import ReactDOM from 'react-dom/client';
// equivalent to
// import { createRoot } from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

// creating a root from the div id=root in index.html
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // injecting JSX
  <div className="p-3 mb-2 bg-dark text-white">
  <BrowserRouter>
  {/* <React.StrictMode> */}
    <App />
  {/* </React.StrictMode> */}
  </BrowserRouter>
  </div>
);
