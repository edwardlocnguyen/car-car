from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

class Technician(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    employee_number = models.PositiveSmallIntegerField(null=True, blank=True)

class Appointment(models.Model):
    vin = models.CharField(max_length=17, null=True, blank=True)
    customer = models.CharField(max_length=100, null=True, blank=True)
    date_time = models.DateTimeField(null=True, blank=True)
    reason = models.TextField(null=True, blank=True)
    complete = models.BooleanField(default=False, null=True, blank=True)
    vip_status = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )