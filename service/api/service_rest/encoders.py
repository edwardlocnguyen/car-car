from common.json import ModelEncoder

from .models import Technician, Appointment

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]
    
class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer",
        "date_time",
        "reason",
        "complete",
        "vip_status",
        'technician',
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }