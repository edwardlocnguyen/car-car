import django, os, sys, time, json, requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# not sure why importing api.service_rest.models doesn't work
from service_rest.models import AutomobileVO

def get_auto():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin=auto["vin"],
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_auto()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)

# just a way to call the poll fxn and preventing it be ran multiple times if imported elsewhere
if __name__ == "__main__":
    poll()